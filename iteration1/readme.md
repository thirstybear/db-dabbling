# Background
By now we should have an initial first normal form database structure derived from the original Excel spreadsheet, 
populated with the existing data provided by the Sailing Club. 

The application runs, bootstrapping its own databse structure as it starts. However, this database structure is most 
likely unuseable - it is incredibly inefficient and difficult to work with as requirements are identified. So we will 
now take a small, incremental step, and _refactor_ the application's database structure towards a better architecture, 
Members and Qualifications first.

# Iteration 1 Instructions

Members can hold Qualifications. Create SQL deltas (`src\main\resources\db\migration\V2.0.0__create_qualification_table.sql` 
and `V2.0.1__migrate_qualification_data.sql`) to create the `qualification` table and populate it from the original data. 
HINT: `qualification.sql` and `migrate_qualification.sql`.

Similarly, identify the fields that belong to the sailing club member.  That is, everything except qualifications and boat fields.
These will make up a `member` table. Write delta scripts (`src\main\resources\db\migration\V2.1.0__create_member_table.sql`
and `V2.1.1__migrate_member_data.sql`) to create the `member` table, and migrate the member data from the original 
`sailing_club_first.sql` table.  (The _surrogate key_ `id` that we generated on the initial load is useful here and will 
be used by later migrations too). 
HINT: `member.sql` and `migrate_member.sql`.

Since a Member can have several Qualifications and the same Qualification can be held by many Members (a 
_many-to-many_ relationship), we also want another table to record which qualifications are held by which members
 (`member_qualification` table). Again, create the necessary SQL delta file (`V2.0.2...`) to do this 
 (HINT: `member_qualification.sql`/`migrate_member_qualification.sql`)

 