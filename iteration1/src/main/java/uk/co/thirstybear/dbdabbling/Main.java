package uk.co.thirstybear.dbdabbling;

import org.flywaydb.core.Flyway;

import java.util.Optional;

import static java.lang.System.getenv;

/**
 * A typical Java application that will update its own database when run.
 *
 * Note that some frameworks integrate with Flyway automatically.
 * For example, Spring Boot (see https://tinyurl.com/y68wgamr)
 */

public class Main {

  private static String db_url;
  private static String db_user;
  private static String db_password;

  public static void main(String[] args) {
    setUpFromEnvironment();

    showDebugDatabaseDetails();

    Flyway flyway = Flyway.configure().dataSource(db_url, db_user, db_password).load();
    flyway.migrate();
  }

  private static void showDebugDatabaseDetails() {
    System.out.println("=========================================================================");
    System.out.println("Migrating database at: " + db_url);
    System.out.println("User: " + db_user);
    System.out.println("Password: " + db_password);
    System.out.println("=========================================================================");
  }

  private static void setUpFromEnvironment() {
    db_url = getEnvironmentOrDefault("DB_URL", "jdbc:postgresql://localhost:5432/yachtclub");
    db_user = getEnvironmentOrDefault("DB_USER", "app");
    db_password = getEnvironmentOrDefault("DB_PASSWORD", "password");
  }

  private static String getEnvironmentOrDefault(String environmentVariable, String defaultValue) {
    return Optional.ofNullable(getenv(environmentVariable)).orElse(defaultValue);
  }
}
