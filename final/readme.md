# The Sailing Club - Final Solution

This project provides a possible complete solution to the exercises.

## Further Exercises/Things to Think About

* We have assumed that each boat belongs to exactly one member. What if there is joint ownership by _n_ people? 
Do this with the final structure and with the un-normalised sailing_club_excel table (or even with the spreadsheet).  
Which is easier?

* We want to record the dates on which members were awarded their qualifications.  Where would we record this date?

* All the qualifications we have are from the Royal Yachting Association (RYA).  What would we do to be able to record 
qualifications from other bodies, maybe recognised qualifications from other countries?

* We have a lot of unused columns and tables (deliberately!) left lying around as we have iterated the database design.
Is this wise? When should these be tidied up? 

* What happens when you modify an SQL delta script after it has been applied? What are the implications to how you need
to approach releases? Version control?

* Microservices use databases. And there might be many services sharing one database. But there is no 
guarantee that all services will be upgraded at the same time. Self-upgrade means the first service to update will
upgrade the database architecture when the others are not yet ready. How could staged service rollout work with zero 
downtime? 


