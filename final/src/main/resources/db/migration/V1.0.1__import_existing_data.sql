set datestyle="DMY";

copy sailing_club_excel
    (
     first_name,
     surname,
     title,
     date_joined,
     address_line_1,
     address_line_2,
     town,
     postcode,
     locker_number,
     qualification,
     boat_1,
     boat_2,
     boat_3
        )
    from
    '/dbdabbling/iteration0/original-files/sailing_club.csv'
    with csv header;
