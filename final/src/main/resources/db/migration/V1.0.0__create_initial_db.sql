create table sailing_club_excel
(
    id                          serial        not null,
    first_name                  varchar(16),
    surname                     varchar(32),
    title                       varchar(16),
    date_joined                 date,
    address_line_1              varchar(32),
    address_line_2              varchar(32),
    town                        varchar(20),
    postcode                    varchar(8),
    locker_number               varchar(5),
    qualification               varchar(200),
    boat_1                      varchar(32),
    boat_2                      varchar(32),
    boat_3                      varchar(32),
    constraint primary_key_sailing_club_excel  primary key (id)
);
