insert into boat
(
    class,
    sail_number,
    racing_handicap,
    member_id
)
select
    boat_1_class as class, 
    boat_1_sail_number as sail_number,
    boat_1_racing_handicap as racing_handicap,
    id as member_id 
from
    sailing_club_first 
where 
    boat_1_class is not null
union
select
    boat_2_class as class, 
    boat_2_sail_number as sail_number,
    boat_2_racing_handicap as racing_handicap,
    id as member_id 
from
    sailing_club_first 
where 
    boat_2_class is not null
union
select
    boat_3_class as class, 
    boat_3_sail_number as sail_number,
    boat_3_racing_handicap as racing_handicap,
    id as member_id 
from
    sailing_club_first 
where 
    boat_3_class is not null;