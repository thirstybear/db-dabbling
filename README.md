# Iterative Database Design - AotB 2019
 
A simple walkthrough of iteratively refactoring a relational database architecture.

## What's used?

* Java 8+
* [Gradle](http://www.gradle.org)
* [Flyway](https://flywaydb.org)
* [Docker](https://www.docker.com) (used to launch a Postgres instance)
* [Postgres](https://www.postgresql.org) (no need to download - we load it using Docker)
* psql (you will either need to install this, or it is installed in the Docker container)

Obviously other languages and frameworks are available. Just not here :D 

## General Principles

* Start simple and _iterate_
* All database structural change is version controlled. **_NO_** human intervention!
* Use an automated framework to apply missing scripts (and let you know if the DB structure is corrupted)
* Let the application apply its own changes
* Don't second guess the database internal optimisations with clever structures. 
    * The folks designing the optimisation algorithms are mostly more specialised and smarter than you are
    * 1st, 2nd and 3rd Normal Forms are your friends. They help the database be fast & efficient.

## Not Covered

### Testing
This is purely a time-constrained (90 minute) exercise in database refactoring. As such there are no unit tests, 
although there are absolutely no reasons why each step could not be test driven using tools such as 
[DBUnit](http://dbunit.wikidot.com), [Fit](http://fit.c2.com), [Fitnesse](http://www.fitnesse.org),
[Spring Boot's various DB test annotations](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html) 
and so on.  