\echo Next query should return zero
select
    a.surname,
    c.qualification
from
    member a
    join
    member_qualification b
    on a.id = b.member_id
    join
    qualification c
    on b.qualification_id = c.id
except
select surname, qualification
from
    (
        select surname, qualification_1 as qualification from sailing_club_first
        union
        select surname, qualification_2 as qualification from sailing_club_first
        union
        select surname, qualification_3 as qualification from sailing_club_first
        union
        select surname, qualification_4 as qualification from sailing_club_first
    ) x
order by 1,2;

select
    a.surname,
    c.qualification
from
    member a
    join
    member_qualification b
    on a.id = b.member_id
    join
    qualification c
    on b.qualification_id = c.id
order by 1,2
limit 30;



