drop table if exists boat;
drop table if exists member_qualification;
drop table if exists qualification;
drop table sailing_club_excel if exists sailing_club_excel;
drop table if exists sailing_club_first;
drop table if exists member;
drop table if exists flyway_schema_history;
