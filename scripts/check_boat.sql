select
    a.surname,
    b.class,
    b.sail_number,
    b.racing_handicap
from
    member a
    join
    boat b
    on a.id = b.member_id
order by 1,2
limit 30;


select 
    class,
    sail_number,
    count(*)
from
    boat
group by
    class,
    sail_number
    having count(*) > 1;

select 
    a.first_name,
    a.surname,
    b.class,
    b.sail_number
from
    member a
    join
    boat b
    on a.id = b.member_id
    join
    (
        select 
            class,
            sail_number,
            count(*)
        from
            boat
        group by
            class,
            sail_number
            having count(*) > 1
    ) c
    on 
        b.class = c.class and
        b.sail_number = c.sail_number
order by 3, 4, 1, 2;