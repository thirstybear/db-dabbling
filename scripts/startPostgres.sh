#!/usr/bin/env bash
DIR="$( cd "$( dirname "$0" )" && pwd )"

docker run --name postgres -e POSTGRES_USER=app -e POSTGRES_PASSWORD=password -e POSTGRES_DB=yachtclub -e PGDATESTYLE=DMY -p 5432:5432 -v ${DIR}/..:/dbdabbling -d --rm postgres
