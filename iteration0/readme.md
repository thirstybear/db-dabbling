# The Sailing Club

## What is in here?
### `original-files`
| File               | What is it?                                    |
|--------------------|------------------------------------------------|
|`sailing_club.xlsx` | Original Sailing Club Excel spreadsheet        |
|`sailing_club.csv`  | Sailing Club Excel spreadsheet exported as CSV |

## Background
The local Sailing Club has just got in touch. They want to update their Excel spreadsheet of members to a more 
modern application. You have agreed to do the work, and in order to leave design decisions to the last responsible 
moment, the work will be carried out iteratively. 

## Iteration 0 Instructions

Take the Excel spreadsheet and get the information into a simple database.

1. Export the data from Excel into a CSV file (this has been done for you: `original-files/sailing_club.csv`).
2. Create the first script `src/main/resources/db/migration/V1.0.0__create_initial_db.sql` (note: **2** underscores!) 
to create the initial single table structure (HINT: `cheatsheet/sailing_club_excel.sql`)
3. Start the database (`../scripts/startPostgres.sh`). 
4. Compile and run the application 
   1. `./gradlew clean run`
   2. Or use an IDE...
5. Now you can create the second script to load the data: `src/main/resources/db/migration/V1.0.1__import_existing_data.sql`
(HINT: `cheatsheet/import_sailing_club_csv.sql`). Run the app to update the database.
6. Repeat to create the 1st Normal Form database structure, and migrate the existing data to it 
(`V1.1.0__create_1NF_table.sql` and `V1.1.1__migrate_data_to_1NF.sql`)
HINT: `cheatsheet/sailing_club_first.sql` and `migrate_sailing_club_first.sql`. 

### Removing the comma separated Qualifications
Looking at the source data, and the database data, there is a single `qualification` column that holds a member's 
qualification list as a comma separated list. This approach is extremely inefficient, and needs to be avoided if using
a RDBMS  system. Create the second SQL delta `src/main/resources/db/migration/V1.0.1__split_qualifications.sql` which 
will split up the data into 4 new columns, and delete the old one (don't worry if your SQL mojo is not great - we provide
the hints you need - for example `cheatsheet/splitqualifications.sql`!).

## Using the provided Dockerised Postgres instance
Postgres has published a Dockerised version of the database, which is _extremely_ convenient for testing, especially if 
you do not want to install it directly on your machine.

### Starting Postgres
```
../scripts/startPostgres.sh
```

Note that the script starts the database with the `PGDATESTYLE` parameer set to UK date format (`DMY`)!

For convenience, the script will also map the top level of the project onto the container directory `/dbdabbling`, so you can use any 
scripts & SQL in there. (HINT: for example, `/dbdabbling/iteration0/cheatsheet/import_sailing_club_csv.sql`...)

### Accessing psql inside the container
1. Log into the container: `docker exec -it postgres bash`
2. Run psql in the container: `psql -h localhost -U app yachtclub`
