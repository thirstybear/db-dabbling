create table boat
(
    id                                    serial not null,
    class                                 varchar(20),
    sail_number                           integer,
    racing_handicap                       integer,
    member_id                             integer,
    constraint primary_key_boat           primary key (id),
    constraint foreign_key_boat_to_member foreign key (member_id) references member(id)
);