create table qualification
(
    id                                    serial        not null,
    qualification                         varchar(50),
    constraint primary_key_qualification  primary key (id)
);
