create table member_qualification
(
    id                   serial        not null,
    member_id            integer,
    qualification_id     integer,
    constraint primary_key_member_qualification  primary key (id),
    constraint foreign_key_member_qualification_to_member
        foreign key (member_id) references member(id),
    constraint foreign_key_member_qualification_to_qualification
        foreign key (qualification_id) references qualification(id)
);
