insert into member_qualification
(
    member_id,
    qualification_id
)
select
    a.member_id,
    b.id
from
    (
        select id as member_id, qualification_1 as qualification from sailing_club_first
        union
        select id as member_id, qualification_2 as qualification from sailing_club_first
        union
        select id as member_id, qualification_3 as qualification from sailing_club_first
        union
        select id as member_id, qualification_4 as qualification from sailing_club_first
    ) a
        join
    qualification b
    on a.qualification = b.qualification
where
        a.qualification <> '';

