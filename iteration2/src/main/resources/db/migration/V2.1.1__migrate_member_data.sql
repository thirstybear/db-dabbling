insert into member
(
    id,            -- we're starting with the id's from sailing_club_first so we can link the other tables.
    first_name,
    surname,
    title,
    date_joined,
    address_line_1,
    address_line_2,
    town,
    postcode,
    locker_number
)
select
    id,
    first_name,
    surname,
    title,
    date_joined,
    address_line_1,
    address_line_2,
    town,
    postcode,
    locker_number
from
    sailing_club_first;
