create table sailing_club_first
(
    id                           serial        not null,
    first_name                   varchar(16),
    surname                      varchar(32),
    title                        varchar(16),
    date_joined                  date,
    address_line_1               varchar(32),
    address_line_2               varchar(32),
    town                         varchar(20),
    postcode                     varchar(8),
    locker_number                varchar(5),
    qualification_1              varchar(50),
    qualification_2              varchar(50),
    qualification_3              varchar(50),
    qualification_4              varchar(50),
    boat_1_class                 varchar(20),
    boat_1_sail_number           integer,
    boat_1_racing_handicap       integer,
    boat_2_class                 varchar(20),
    boat_2_sail_number           integer,
    boat_2_racing_handicap       integer,
    boat_3_class                 varchar(20),
    boat_3_sail_number           integer,
    boat_3_racing_handicap       integer,
    constraint primary_key_sailing_club_first primary key (id)
);
