# Background

We now have a database where the Member and Qualification data has been refactored to their own tables, and the 
many-to-many relation between Member and Qualification is represented table using a mapping table.

Now to sort out the Boats... 

# Iteration 2 Instructions

The `sail_number` and the `racing_handicap` (also charmingly know as the Portsmouth yardstick) depend not on the member 
but on the boat class (class is the term used by sailors and sailing organisations).  So boat must be a separate table.  
It's also a repeating group, so we are taking it from first normal form to third, fixing the second normal form issue on 
the way.  We are making the assumption that a boat belongs to exactly one member, so we don't need a linking table, we 
just have a foreign key from boat to member. 

Write the necessary `V3.x.x....` deltas to migrate the database to the new structure.

HINT: `boat.sql` and `migrate_boat.sql`.
